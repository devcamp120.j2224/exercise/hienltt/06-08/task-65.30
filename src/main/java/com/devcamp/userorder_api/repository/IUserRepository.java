package com.devcamp.userorder_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.userorder_api.model.CUser;

public interface IUserRepository extends JpaRepository<CUser, Long>{
    CUser findById(long id);
}
